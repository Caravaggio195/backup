#!/bin/bash
#script backup
#Author: Domenico Caravaggio
DATE=$(date +%d-%m-%Y)
BACKUP_DIR="/home"
BACKUP_DEST="/mnt/backup/home"
#New directory in the backup
DIRECTORY=$BACKUP_DEST/$DATE
if [ ! -d "$DIRECTORY" ]; then
    # Will enter here if $DIRECTORY not exists, even if it contains spaces
    $BACKUP_DEST/$DATE
    mkdir -p $BACKUP_DEST/$DATE
    tar -zcvpf $BACKUP_DEST/$DATE/$DATE.tar.gz $BACKUP_DIR
#To delete files older than 10 days
elif [ ! -f $BACKUP_DEST/$DATE/$DATE.tar.gz ]; then
    tar -zcvpf $BACKUP_DEST/$DATE/$DATE.tar.gz $BACKUP_DIR
else
    echo "exists $BACKUP_DEST/$DATE/$DATE.tar.gz"
fi
find $BACKUP_DEST/* -mtime +5 -exec rm -rf {} \;
echo "Fine"
exit 0




